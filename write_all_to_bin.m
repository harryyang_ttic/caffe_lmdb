class='aeroplane';

addpath('/share/project/shapes/harry/datahandling');
ds=mapDataSets('voc12','fg-all',class);

maskpath=['/share/data/vision-greg/harry/',class];
savepath=['/share/data/vision-greg/harry/',class,'_bin'];

if ~exist(savepath)
    mkdir(savepath);
end

myfile2=fopen(fullfile(savepath,'filelist.txt'),'w');
 
for i=1:length(ds.imnames)
    mask_mat=load(fullfile(maskpath,[ds.imnames{i},'_pos.mat']));
    mask_res=mask_mat.mask_res;
    num=length(mask_res);
     fprintf(myfile2,'%s %d 1\n',[ds.imnames{i},'_pos.bin'],num);
      mask_mat=load(fullfile(maskpath,[ds.imnames{i},'_neg.mat']));
    mask_res=mask_mat.mask_res;
    num=length(mask_res);
     fprintf(myfile2,'%s %d 0\n',[ds.imnames{i},'_neg.bin'],num);
end

fclose(myfile2);

for i=1:length(ds.imnames)
    mat=load(fullfile(ds.spdirs.slic,[ds.imnames{i},'_slic_k600_m15.mat']));
    im=mat.im;
    
    mask_mat=load(fullfile(maskpath,[ds.imnames{i},'_pos.mat']));
    mask_res=mask_mat.mask_res;
    pos_res=mask_mat.pos_res;
    
    myfile=fopen(fullfile(savepath,[ds.imnames{i},'_pos.bin']),'w');
    num=length(mask_res);
    for j=1:num
        p=cell2mat(pos_res(j));
        maskj=cell2mat(mask_res(j));
        
        im_patch=im(p(1):p(1)+34,p(2):p(2)+34,:);
        maskr=repmat(maskj,[1,1,3]);
        A=cat(3,im_patch.*uint8(maskr),im_patch.*uint8((1-maskr)));
        fwrite(myfile,A);
    end
    fclose(myfile);
    fprintf(myfile2,'%s %d 1\n',[ds.imnames{i},'_pos.bin'],num);
    
    
    mask_mat=load(fullfile(maskpath,[ds.imnames{i},'_neg.mat']));
    mask_res=mask_mat.mask_res;
    pos_res=mask_mat.pos_res;
    
    myfile=fopen(fullfile(savepath,[ds.imnames{i},'_neg.bin']),'w');
    num=length(mask_res);
    for j=1:num
        p=cell2mat(pos_res(j));
        maskj=cell2mat(mask_res(j));
        
        im_patch=im(p(1):p(1)+34,p(2):p(2)+34,:);
        maskr=repmat(maskj,[1,1,3]);
        A=cat(3,im_patch.*uint8(maskr),im_patch.*uint8((1-maskr)));
        fwrite(myfile,A);
    end
    fclose(myfile);
    fprintf(myfile2,'%s %d 0\n',[ds.imnames{i},'_neg.bin'],num);
end
fclose(myfile2);

