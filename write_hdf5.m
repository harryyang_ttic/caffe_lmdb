hdf5_name='sample_weight.h5';

if exist(hdf5_name,'file')
   delete(hdf5_name);   
end

h5create(hdf5_name,'/sample_weight',Inf,'Datatype','single','ChunkSize',1);

fid=fopen('filelist.txt');
C=textscan(fid,'%s %d %d');

num=length(C{2});

patch_id=1;
for i=1:num
    fprintf('%d\n',i);
    pnum=C{2}(i); 
    label=C{3}(i);
    if label==1
        w=single(1);
    else
        w=single(0.01);
    end
    for j=1:pnum
        h5write(hdf5_name,'/sample_weight',w, patch_id, 1);
        patch_id=patch_id+1;
    end
end